package com.andr2i.androidcalculator.presentation.main;


import com.andr2i.androidcalculator.Screens;
import com.andr2i.androidcalculator.model.Calculator;
import com.andr2i.androidcalculator.model.ICalculator;
import com.andr2i.androidcalculator.presentation.Router;

//
public class MainPresenter implements MainContract.Presenter {

    private MainContract.View view;
    private ICalculator model;


    public MainPresenter() {
        this.model = new Calculator();

    }


    @Override
    public void startView(MainContract.View view) {
       this.view = view;
    }

    @Override
    public void detachView() {
        if (view != null)
            view = null;
    }


    @Override
    public void init() {
       // Router.getInstanse().transactionCalcFragment();
        Router.getInstanse().navigateTo(new Screens.CalcScreen());
    }


}
