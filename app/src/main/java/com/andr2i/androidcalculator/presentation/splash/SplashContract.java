package com.andr2i.androidcalculator.presentation.splash;

import com.andr2i.androidcalculator.presentation.base.BaseActivityContract;
import com.andr2i.androidcalculator.presentation.base.BasePresenter;

public interface SplashContract {
    interface View {
        void message(String s);
//interface View extends BaseActivityContract {
    }

    interface Presenter extends BasePresenter<View> {
        void pause();
    }
}
