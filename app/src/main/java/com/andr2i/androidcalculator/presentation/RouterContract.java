package com.andr2i.androidcalculator.presentation;


import com.andr2i.androidcalculator.Screen;
import com.andr2i.androidcalculator.presentation.base.BaseActivity;
import com.andr2i.androidcalculator.presentation.base.BaseActivityContract;

public interface RouterContract {

    void onStart(BaseActivity view, int containerId);

    void navigateTo(Screen screen);

    //void transactionCalcFragment();

   // void transactionMainActivity();

//    void backStack();
//
//    void isBack(boolean val);
//
//    void header(String val);
//
//    void message(String val);
//
//    void isProgress(boolean val);

}
