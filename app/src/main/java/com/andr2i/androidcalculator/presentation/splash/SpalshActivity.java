package com.andr2i.androidcalculator.presentation.splash;


import androidx.fragment.app.Fragment;


import com.andr2i.androidcalculator.R;
import com.andr2i.androidcalculator.databinding.ActivitySpalshBinding;
import com.andr2i.androidcalculator.presentation.Router;
import com.andr2i.androidcalculator.presentation.base.BaseActivity;
import com.andr2i.androidcalculator.presentation.base.BasePresenter;


public class SpalshActivity extends BaseActivity<ActivitySpalshBinding>implements SplashContract.View {
    private SplashContract.Presenter presenter;

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_spalsh;
    }

    @Override
    protected void initView() {
        Router.getInstanse().onStart(this, R.id.content);
        presenter = new SplashPresenter();
        presenter.pause();
    }

    @Override
    protected void onStartView() {
        presenter.startView(this);
    }

    @Override
    protected void onDestroyView() {
        presenter.detachView();
    }

    @Override
    protected BasePresenter getPresenter() {
        return presenter;
    }

    @Override
    public void transactionFragmentNoBackStack(Fragment fragment, int container) {

    }

    @Override
    public void transactionFragmentWithBackStack(Fragment fragment, int container) {

    }

    @Override
    public void transactionActivity(Class<?> activity, boolean cycleFinish) {
        super.transactionActivity(activity,cycleFinish);
    }

    @Override
    public <T> void transactionActivity(Class<?> activity, boolean cycleFinish, T... object) {
        super.transactionActivity(activity,cycleFinish,object);
    }
//
    @Override
    public void message(String val) {
        toast(val);
    }
//
//    @Override
//    public void isProgress(boolean val) {
//
//    }
//
//    @Override
//    public void backStack() {
//
//    }
//
//    @Override
//    public void isBack(boolean val) {
//
//    }
//
//    @Override
//    public void header(String val) {
//
//    }
//
//
//    @Override
//    public Context getContext() {
//        return this;
//    }


}