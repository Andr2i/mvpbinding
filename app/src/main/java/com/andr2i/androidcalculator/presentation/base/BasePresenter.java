package com.andr2i.androidcalculator.presentation.base;

public interface BasePresenter<V> {

    void startView(V view);

    void detachView();
}
