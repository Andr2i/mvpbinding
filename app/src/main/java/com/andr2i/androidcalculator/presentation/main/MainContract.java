package com.andr2i.androidcalculator.presentation.main;


import com.andr2i.androidcalculator.presentation.base.BaseActivityContract;
import com.andr2i.androidcalculator.presentation.base.BasePresenter;

public interface MainContract {
    interface View extends BaseActivityContract {


    }

    interface Presenter extends BasePresenter<View> {
        void init();
    }
}
