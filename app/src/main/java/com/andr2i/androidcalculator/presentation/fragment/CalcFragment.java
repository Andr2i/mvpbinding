package com.andr2i.androidcalculator.presentation.fragment;


import android.os.Bundle;


import com.andr2i.androidcalculator.R;
import com.andr2i.androidcalculator.databinding.FragmentCalcBinding;
import com.andr2i.androidcalculator.presentation.base.BaseFragment;
import com.andr2i.androidcalculator.presentation.base.BasePresenter;

public class CalcFragment extends BaseFragment<FragmentCalcBinding> implements CalcFragContract.View {
    private CalcFragContract.Presenter presenter;
    private static final String TAG_DATA = "data";


    public CalcFragment() {

    }


    public static CalcFragment newInstance() {
        CalcFragment fragment = new CalcFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    protected BasePresenter getPresenter() {
        return presenter;
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_calc;
    }

    @Override
    protected void initFragmentView() {
        presenter = new CalcFragPresenter();
        getBinding().setHandlerPresenter(presenter);
    }

    @Override
    protected void attachFragment() {

    }

    @Override
    protected void startFragment() {
        presenter.startView(this);
    }

    @Override
    protected void stopFragment() {

    }

    @Override
    protected void destroyFragment() {
        presenter.detachView();
    }

    @Override
    protected void pauseFragment() {

    }

    @Override
    public void showExpression(String val, boolean error) {
        getBinding().setExpression(val);
        getBinding().setHasError(error);
    }

    @Override
    public void showResult(String val) {
        getBinding().setResult(val);
    }


}
