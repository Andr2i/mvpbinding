package com.andr2i.androidcalculator.presentation.base;


import android.content.Context;
import android.content.Intent;

import androidx.fragment.app.Fragment;

import com.andr2i.androidcalculator.Screen;

public interface BaseActivityContract {
    void transactionFragmentNoBackStack(Fragment fragment, int container);

    void transactionFragmentWithBackStack(Fragment fragment, int container);

    void transactionActivity(Class<?> activity, boolean cycleFinish);

    <T> void transactionActivity(Class<?> activity, boolean cycleFinish, T... object);

    void message(String val);

    void isProgress(boolean val);

    void backStack();

    void isBack(boolean val);

    void header(String val);

    Context getContext();

   // void checkAndStartActivity(Intent activityIntent);

   // void fragmentReplace(Fragment fragment, int containerId);
}
