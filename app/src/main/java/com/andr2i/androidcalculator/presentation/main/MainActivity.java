package com.andr2i.androidcalculator.presentation.main;


import android.content.Context;

import io.reactivex.disposables.Disposable;

import com.andr2i.androidcalculator.R;
import com.andr2i.androidcalculator.Screen;
import com.andr2i.androidcalculator.databinding.ActivityMainBinding;

import com.andr2i.androidcalculator.presentation.Router;
import com.andr2i.androidcalculator.presentation.base.BaseActivity;
import com.andr2i.androidcalculator.presentation.base.BasePresenter;


import androidx.fragment.app.Fragment;

public class MainActivity extends BaseActivity<ActivityMainBinding> implements MainContract.View {

    private MainContract.Presenter presenter;


    public MainActivity()
    {

    }
    @Override
    protected int getLayoutRes() {
        return R.layout.activity_main;
    }

    @Override
    protected void initView() {
        Router.getInstanse().onStart(this,R.id.content);
        presenter = new MainPresenter();
        presenter.init();
    }

    @Override
    protected void onStartView() {
        presenter.startView(this);
    }

    @Override
    protected void onDestroyView() {

    }

    @Override
    protected BasePresenter getPresenter() {
        return presenter;
    }

    @Override
    public void transactionFragmentNoBackStack(Fragment fragment, int container) {
          super.transactionFragmentNoBackStack(fragment,container);
    }

    @Override
    public void transactionFragmentWithBackStack(Fragment fragment, int container) {
        super.transactionFragmentWithBackStack(fragment,container);
    }

    @Override
    public void transactionActivity(Class<?> activity, boolean cycleFinish) {
        super.transactionActivity(activity,cycleFinish);
    }

    @Override
    public <T> void transactionActivity(Class<?> activity, boolean cycleFinish, T... object) {
        super.transactionActivity(activity,cycleFinish,object);
    }

    @Override
    public void message(String val) {
        toast(val);
    }

    @Override
    public void isProgress(boolean val) {

    }

    @Override
    public void backStack() {
        super.onBackPressed();
    }

    @Override
    public void isBack(boolean val) {

    }

    @Override
    public void header(String val) {

    }

    @Override
    public Context getContext() {
        return this;
    }

}
