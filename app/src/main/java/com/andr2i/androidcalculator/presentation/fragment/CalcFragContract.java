package com.andr2i.androidcalculator.presentation.fragment;

import com.andr2i.androidcalculator.presentation.base.BasePresenter;

public interface CalcFragContract {
    interface View  {
        void showExpression(String val, boolean error);
        void showResult(String val);

    }

    interface Presenter extends BasePresenter<View> {
        void onClick(String symbol);

    }
}