package com.andr2i.androidcalculator.presentation.fragment;


import com.andr2i.androidcalculator.domain.CalculatorInteractor;
import com.andr2i.androidcalculator.model.Calculator;
import com.andr2i.androidcalculator.model.ICalculator;

import io.reactivex.observers.DisposableObserver;



public class CalcFragPresenter implements CalcFragContract.Presenter {

    private CalcFragContract.View view;
    private ICalculator model;

    private final CalculatorInteractor calculatorInteractor ;

    public CalcFragPresenter() {
        this.model = new Calculator();
        calculatorInteractor = new CalculatorInteractor(this.model);
    }

    @Override
    public void onClick(String symbol) {
        if ("=".equals(symbol)) {
            String ev = model.getExpression();


        } else {
             model.appendExpression(symbol.charAt(0));

               calculatorInteractor.execute(new DisposableObserver<String>() {

                @Override
                public void onNext(String s) {
                    view.showResult(s);
                    view.showExpression(model.getExpression(), model.hasError());
                }

                @Override
                public void onError(Throwable e) {
                    view.showExpression(e.getMessage(), true);
                }

                @Override
                public void onComplete() {

                }
            });

        }
    }


    @Override
    public void startView(CalcFragContract.View view) {

         this.view = view;
    }

    @Override
    public void detachView() {
        if (view != null) view = null;
        calculatorInteractor.unsubscribe();


    }


}