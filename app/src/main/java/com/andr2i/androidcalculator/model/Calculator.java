package com.andr2i.androidcalculator.model;

import io.reactivex.Observable;


public class Calculator implements ICalculator {

    private ExpressionTree et;

    Expression expression;
    String result;
    boolean error;

    public Calculator() {

        et = new ExpressionTree();
        expression = new Expression("0");
        result = "0";
    }


    @Override
    public String getExpression() {
        return expression.getStringExpression();
    }

    @Override
    public String evalExpression() {

        try {

            if (expression.zerroDivision()) {
                result = "деление на 0";
                error = true;
            } else {
                et.buildTree(expression.getPrefixExpression());
                result = String.valueOf(et.evaluate());
                error = false;
            }

        } catch (Exception e) {
            error = true;
        }

        return result;
    }

    @Override
    public boolean hasError() {
        return error;
    }

    @Override
    public void appendExpression(char c) {
        expression.append(c);
    }


    @Override
    public Observable<String> getResult() {

        try {

            if (expression.zerroDivision()) {
                result = "деление на 0";
                error = true;
            } else {
                et.buildTree(expression.getPrefixExpression());
                result = String.valueOf(et.evaluate());
                error = false;
            }
            return  Observable.just(result);

        } catch (Exception e) {
            return  Observable.error(new Exception("error"));
        }


    }
}
