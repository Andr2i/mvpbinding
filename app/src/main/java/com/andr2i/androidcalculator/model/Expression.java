package com.andr2i.androidcalculator.model;

import java.util.ArrayList;
import java.util.Stack;
import java.util.regex.Pattern;

public class Expression {

    private StringBuilder expression;
    private String digit;

    public Expression(String exp) {
        expression = new StringBuilder(exp);
    }

    public void append(char c) {
        if (c == 'C')
            removeLast();
        else {
            if (isDigit(c))
                appendOperand(c);
            else if (c == '(' || c == ')')
                appendBracket(c);
            else
                appendOperator(c);
        }
    }

    private void appendOperator(char c) {
        if (isOperator(getLast()) && !isBracket(getLast()))
            replaceLast(c);
        else
            expression.append(c);
    }

    private void appendOperand(char c) {
        if (getLast() == '0') {
            if (expression.length() == 1)
                replaceLast(c);
            else if (isOperator(expression.charAt(expression.length() - 2)))
                replaceLast(c);
            else
                expression.append(c);
        } else
            expression.append(c);

    }

    private void appendBracket(char c) {
        if (c == '(') {

            if (getLast() == '0') {
                if (expression.length() == 1)
                    replaceLast(c);
            } else {
                if (isDigit(getLast()))
                    expression.append('*');
                expression.append(c);
            }
        } else {
            if (isOperator(getLast()))
                replaceLast(c);
            else
                expression.append(c);
        }

    }

    private char getLast() {
        return expression.charAt(expression.length() - 1);
    }

    public void removeLast() {
        if (expression.length() > 1)
            expression.deleteCharAt(expression.length() - 1);
        else
            replaceLast('0');
    }

    private void replaceLast(char c) {
        if (expression.length() > 0)
            expression.setCharAt(expression.length() - 1, c);

    }

    public boolean zerroDivision () {
        if (Pattern.matches(".*\\/0([^.]|$|\\.(0{4,}.*|0{1,4}([^0-9]|$))).*", expression))
        {
            return true;
        }
        else {
          return false;
        }
    }

    public String getSymbolExpression() {

        char operand = 'a';
        StringBuilder symbolExpression = new StringBuilder();

        boolean flagDigit = false;

        for (int i = 0; i < expression.length(); i++) {

            char c = expression.charAt(i);
            if (!isDigit(c) && c != '.') {
                if (flagDigit) {
                    symbolExpression.append(operand); //add operand
                    operand++;
                }

                symbolExpression.append(c); //  operator
                flagDigit = false;
            } else {
                flagDigit = true;
            }
        }
        if (flagDigit)
            symbolExpression.append(operand);

        return symbolExpression.toString();
    }

    public String[] getOperands() {

        StringBuilder operand = new StringBuilder();
        boolean flagDigit = false;
        ArrayList<String> arr = new ArrayList<String>();

        for (int i = 0; i < expression.length(); i++) {

            char c = expression.charAt(i);
            if (isDigit(c) || c == '.') {
                operand.append(c);
            } else {
                if (operand.length() > 0) {
                    arr.add(operand.toString());
                    operand.setLength(0);
                }
            }
        }
        if (operand.length() > 0)
            arr.add(operand.toString());

        return arr.toArray(new String[0]);
    }

    public String getStringExpression() {
        return expression.toString();
    }

    public String getPrefixExpression() {
        return infixToPrefix();
    }


    public void remove() {
        expression.deleteCharAt(expression.length() - 1);
    }

    private boolean isOperator(char c) {
        return (!(c >= 'a' && c <= 'z') &&
                !(c >= '0' && c <= '9') &&
                !(c >= 'A' && c <= 'Z'));
    }

    private boolean isBracket(char c) {
        return c == '(' || c == ')';
    }

    private boolean isDigit(char c) {
        return ((c >= '0' && c <= '9'));
    }

    // Function to find priority
// of given operator.
    static int getPriority(char C) {
        if (C == '-' || C == '+')
            return 1;
        else if (C == '*' || C == '/')
            return 2;
        else if (C == '^')
            return 3;
        return 0;
    }

    // Function that converts infix
// expression to prefix expression.
    private String infixToPrefix() {

        String infix = getSymbolExpression();
        String[] operandsValue = getOperands();
        // stack for operators.
        Stack<Character> operators = new Stack<Character>();

        // stack for operands.
        Stack<String> operands = new Stack<String>();

        for (int i = 0; i < infix.length(); i++) {

            // If current character is an
            // opening bracket, then
            // push into the operators stack.
            if (infix.charAt(i) == '(') {
                operators.push(infix.charAt(i));

            }

            // If current character is a
            // closing bracket, then pop from
            // both stacks and push result
            // in operands stack until
            // matching opening bracket is
            // not found.
            else if (infix.charAt(i) == ')') {
                while (!operators.empty() &&
                        operators.peek() != '(') {

                    // operand 1
                    String op1 = operands.peek();
                    operands.pop();

                    // operand 2
                    String op2 = operands.peek();
                    operands.pop();

                    // operator
                    char op = operators.peek();
                    operators.pop();

                    // Add operands and operator
                    // in form operator +
                    // operand1 + operand2.
                    String tmp = op + op2 + op1;
                    operands.push(tmp);
                }

                // Pop opening bracket
                // from stack.
                operators.pop();
            }

            // If current character is an
            // operand then push it into
            // operands stack.
            else if (!isOperator(infix.charAt(i))) {
                operands.push(infix.charAt(i) + "");

            }

            // If current character is an
            // operator, then push it into
            // operators stack after popping
            // high priority operators from
            // operators stack and pushing
            // result in operands stack.
            else {
                while (!operators.empty() &&
                        getPriority(infix.charAt(i)) <=
                                getPriority(operators.peek())) {

                    String op1 = operands.peek();
                    operands.pop();

                    String op2 = operands.peek();
                    operands.pop();

                    char op = operators.peek();
                    operators.pop();

                    String tmp = op + op2 + op1;
                    operands.push(tmp);
                }

                operators.push(infix.charAt(i));
            }
        }

        // Pop operators from operators
        // stack until it is empty and
        // operation in add result of
        // each pop operands stack.
        while (!operators.empty()) {
            String op1 = operands.peek();
            operands.pop();

            String op2 = operands.peek();
            operands.pop();

            String op = Character.toString(operators.peek());
            operators.pop();

            String tmp = op + op2 + op1;
            operands.push(tmp);
        }

        // Final prefix expression is
        // present in operands stack.
        String expr = operands.peek();

        StringBuilder splitexpr = new StringBuilder();

        for (int i = 0; i < expr.length(); i++) {

            char c = expr.charAt(i);
            if (isOperator(c)) {
                splitexpr.append(c);
                splitexpr.append(";");
            } else {
                String value = operandsValue[c - 'a'];
                splitexpr.append(value);
                splitexpr.append(";");
            }
        }

        return splitexpr.toString();

    }
}
