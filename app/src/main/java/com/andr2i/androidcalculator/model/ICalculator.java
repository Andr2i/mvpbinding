package com.andr2i.androidcalculator.model;


import com.andr2i.androidcalculator.domain.CalculatorDataProvider;


public interface ICalculator extends CalculatorDataProvider {

    String getExpression();

    void appendExpression(char c);

    String evalExpression();

    boolean hasError();

}
