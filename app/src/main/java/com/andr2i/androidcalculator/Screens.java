package com.andr2i.androidcalculator;

import android.content.Context;
import android.content.Intent;

import androidx.fragment.app.Fragment;

import com.andr2i.androidcalculator.presentation.fragment.CalcFragment;
import com.andr2i.androidcalculator.presentation.main.MainActivity;

public class Screens {


    public static final class CalcScreen extends Screen {

        @Override
        public Fragment getFragment() {
            return CalcFragment.newInstance();
        }
    }

    public static final class MainScreen extends Screen {
        @Override

        public Intent getActivityIntent(Context context) {
            return new Intent(context, MainActivity.class);
        }
    }

}
