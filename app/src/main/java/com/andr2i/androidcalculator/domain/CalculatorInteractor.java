package com.andr2i.androidcalculator.domain;



import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;


public class CalculatorInteractor extends Interactor<String, Void> {

    private final CalculatorDataProvider calculatorsDataProvider;


    public CalculatorInteractor(CalculatorDataProvider calculatorsDataProvider)
    {
        super(Schedulers.io(), AndroidSchedulers.mainThread());
        this.calculatorsDataProvider = calculatorsDataProvider;
    }

    @Override
    protected Observable<String> buildObservable(Void parameter) {
        return calculatorsDataProvider.getResult();
    }
}