package com.andr2i.androidcalculator.domain;

import io.reactivex.Observable;

public interface CalculatorDataProvider {
    Observable<String> getResult();
}
